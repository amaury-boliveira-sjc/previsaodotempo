package br.com.previsaodotempo.api;

import br.com.previsaodotempo.model.City;
import br.com.previsaodotempo.model.CityForecast;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ForecastAPI {

    @GET("listaCidades")
    Call<City> listCity(@Query("city") String city);

    @GET("cidade/7dias/{id}/previsao.xml")
    Call<CityForecast> forecast(@Path("id") long id);

}
