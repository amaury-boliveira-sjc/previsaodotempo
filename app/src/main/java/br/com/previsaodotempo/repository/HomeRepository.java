package br.com.previsaodotempo.repository;

import android.util.Log;

import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import br.com.previsaodotempo.BuildConfig;
import br.com.previsaodotempo.api.ForecastAPI;
import br.com.previsaodotempo.model.City;
import br.com.previsaodotempo.model.CityForecast;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class HomeRepository {

    private ForecastAPI forecastAPI;

    public HomeRepository() {
        this.forecastAPI = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(new Persister(new AnnotationStrategy())))
                .build().create(ForecastAPI.class);
    }

    public void listCity(String city, final ListCityCallback callback){
        Call<City> call = forecastAPI.listCity(city);
        call.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (response.isSuccessful()){
                    callback.onSuccess(response.body());
                }else{
                    callback.onFailure("Erro ao consultar cidade");
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                Log.e("error", Log.getStackTraceString(t));
                callback.onFailure("Error ao consultar cidade");
            }
        });
    }

    public void forecast(long id, final ForecastCallback callback){
        Call<CityForecast> call = forecastAPI.forecast(id);
        call.enqueue(new Callback<CityForecast>() {
            @Override
            public void onResponse(Call<CityForecast> call, Response<CityForecast> response) {
                if (response.isSuccessful()){
                    callback.onSuccess(response.body());
                }else{
                    callback.onFailure("Erro ao listar previsão para essa cidade");
                }
            }

            @Override
            public void onFailure(Call<CityForecast> call, Throwable t) {
                Log.e("error", Log.getStackTraceString(t));
                callback.onFailure("Erro ao listar previsão para essa cidade");
            }
        });
    }

    public interface ListCityCallback {

        void onSuccess(City city);

        void onFailure(String error);
    }

    public interface ForecastCallback{

        void onSuccess(CityForecast cityForecast);

        void onFailure(String error);
    }
}
