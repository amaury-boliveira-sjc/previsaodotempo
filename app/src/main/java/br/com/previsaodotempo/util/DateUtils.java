package br.com.previsaodotempo.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static String format(String date, String currentPattern, String newPattern) {
        try {
            Date parsedDate = new SimpleDateFormat(currentPattern, Locale.getDefault()).parse(date);
            return new SimpleDateFormat(newPattern, Locale.getDefault()).format(parsedDate);
        } catch (Exception e) {
            Log.e("DateUtils", Log.getStackTraceString(e));
            return null;
        }
    }
}
