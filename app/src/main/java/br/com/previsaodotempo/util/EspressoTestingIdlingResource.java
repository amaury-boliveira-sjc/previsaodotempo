package br.com.previsaodotempo.util;

import androidx.test.espresso.idling.CountingIdlingResource;

public class EspressoTestingIdlingResource {

    private static CountingIdlingResource resource = new CountingIdlingResource("GLOBAL");

    public static CountingIdlingResource getResource() {
        return resource;
    }

    public static void increment(){
        resource.increment();
    }

    public static void decrement(){
        resource.decrement();
    }
}
