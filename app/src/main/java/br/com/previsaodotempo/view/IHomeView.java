package br.com.previsaodotempo.view;

import br.com.previsaodotempo.model.CityForecast;

public interface IHomeView {
    void showForecasts(CityForecast cityForecast);

    void showError(String error);
}
