package br.com.previsaodotempo.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import br.com.previsaodotempo.R;
import br.com.previsaodotempo.model.Forecast;
import br.com.previsaodotempo.util.DateUtils;

public class WeatherForecastAdapter extends RecyclerView.Adapter<WeatherForecastAdapter.ForecastViewHolder> {

    private List<Forecast> forecastList;

    public WeatherForecastAdapter(List<Forecast> forecastList) {
        this.forecastList = forecastList;
    }

    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_predictions, parent, false);
        return new ForecastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder holder, int position) {
        holder.bind(forecastList.get(position));
    }

    @Override
    public int getItemCount() {
        return forecastList.size();
    }


    public class ForecastViewHolder extends RecyclerView.ViewHolder{

        private CardView cardView;
        private TextView textViewDate;
        private TextView textViewCondition;
        private TextView textViewTemperatures;

        public ForecastViewHolder(@NonNull View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.card_view_weather_forecast);
            this.textViewDate = itemView.findViewById(R.id.text_view_date);
            this.textViewCondition = itemView.findViewById(R.id.text_view_condition);
            this.textViewTemperatures = itemView.findViewById(R.id.text_view_temperatures);
        }

        public void bind(Forecast forecast){
            textViewDate.setText(DateUtils.format(forecast.getDate(), "yyyy-MM-dd", "dd/MM/yyyy"));
            textViewCondition.setText(getCondition(forecast.getCondition()));
            textViewTemperatures.setText(String.format("%s°C/%s°C", forecast.getMaxTemperature(), forecast.getMinTemperature()));
        }

        private String getCondition(String initials){
            String condition;
            switch (initials){
                case "ec" :
                    return condition = "Encoberto com Chuvas Isoladas";
                case "ci" :
                    return condition = "Chuvas Isoladas";
                case "c" :
                    return condition = "Chuva";
                case "in" :
                    return condition = "Instável";
                case "pp" :
                    return condition = "Poss. de Pancadas de Chuva";
                case "cm" :
                    return condition = "Chuva pela Manhã";
                case "cn" :
                    return condition = "Chuva a Noite";
                case "pt" :
                    return condition = "Pancadas de Chuva a Tarde";
                case "pm" :
                    return condition = "Pancadas de Chuva pela Manhã";
                case "np" :
                    return condition = "Nublado e Pancadas de Chuva";
                case "pc" :
                    return condition = "Pancadas de Chuva";
                case "pn" :
                    return condition = "Parcialmente Nublado";
                case "cv" :
                    return condition = "Chuvisco";
                case "ch" :
                    return condition = "Chuvoso";
                case "t" :
                    return condition = "Tempestade";
                case "ps" :
                    return condition = "Predomínio de Sol";
                case "e" :
                    return condition = "Encoberto";
                case "n" :
                    return condition = "Nublado";
                case "cl" :
                    return condition = "Céu Claro";
                case "nv" :
                    return condition = "Nevoeiro";
                case "g" :
                    return condition = "Geada";
                case "ne" :
                    return condition = "Neve";
                case "pnt" :
                    return condition = "Pancadas de Chuva a Noite";
                case "psc" :
                    return condition = "Possibilidade de Chuva";
                case "pcm" :
                    return condition = "Possibilidade de Chuva pela Manhã";
                case "pct" :
                    return condition = "Possibilidade de Chuva a Tarde";
                case "pcn" :
                    return condition = "Possibilidade de Chuva a Noite";
                case "npt" :
                    return condition = "Nublado com Pancadas a Tarde";
                case "npn" :
                    return condition = "Nublado com Pancadas a Noite";
                case "ncn" :
                    return condition = "Nublado com Poss. de Chuva a Noite";
                case "nct" :
                    return condition = "Nublado com Poss. de Chuva a Tarde";
                case "ncm" :
                    return condition = "Nubl. c/ Poss. de Chuva pela Manhã";
                case "npm" :
                    return condition = "Nublado com Pancadas pela Manhã";
                case "npp" :
                    return condition = "Nublado com Possibilidade de Chuva";
                case "vn" :
                    return condition = "Variação de Nebulosidade";
                case "ct" :
                    return condition = "Chuva a Tarde";
                case "ppn" :
                    return condition = "Poss. de Panc. de Chuva a Noite";
                case "ppt" :
                    return condition = "Poss. de Panc. de Chuva a Tarde";
                case "ppm" :
                    return condition = "Poss. de Panc. de Chuva pela Manhã";
                default:
                    return condition = "Não Definido";
            }
        }
    }
}
