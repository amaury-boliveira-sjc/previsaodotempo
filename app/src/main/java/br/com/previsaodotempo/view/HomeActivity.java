package br.com.previsaodotempo.view;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;

import br.com.previsaodotempo.R;
import br.com.previsaodotempo.model.CityForecast;
import br.com.previsaodotempo.presenter.HomePresenter;
import br.com.previsaodotempo.repository.HomeRepository;
import br.com.previsaodotempo.util.DateUtils;
import br.com.previsaodotempo.util.EspressoTestingIdlingResource;
import br.com.previsaodotempo.view.adapter.WeatherForecastAdapter;

public class HomeActivity extends AppCompatActivity implements Button.OnClickListener, IHomeView {

    private TextInputEditText editTextCityName;
    private TextView textViewCity;
    private TextView textViewUpdated;
    private RecyclerView recyclerView;
    private Button buttonSearch;
    private WeatherForecastAdapter forecastAdapter;
    private HomePresenter homePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initComponents();
        initListeners();
    }

    private void initComponents(){
        this.editTextCityName = findViewById(R.id.edit_text_city_name);
        this.textViewCity = findViewById(R.id.text_view_city);
        this.textViewUpdated = findViewById(R.id.text_view_updated);
        this.recyclerView = findViewById(R.id.recycler_view_weather_forecast);
        this.buttonSearch = findViewById(R.id.button_search);
        this.homePresenter = new HomePresenter(this, new HomeRepository());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initListeners(){
        this.buttonSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        homePresenter.getCityID(editTextCityName.getText().toString());

        //logica de espera de método assincrono apenas para testes
        EspressoTestingIdlingResource.increment();
    }

    @Override
    public void showForecasts(CityForecast cityForecast){
        //logica de espera de método assincrono apenas para testes
        EspressoTestingIdlingResource.decrement();

        editTextCityName.setText("");
        textViewCity.setText(getString(R.string.text_view_city, Html.fromHtml(cityForecast.getName())));
        textViewUpdated.setText(getString(R.string.text_view_updated_date,
                DateUtils.format(cityForecast.getUpdatedDate(), "yyyy-MM-dd", "dd/MM/yyyy")));
        forecastAdapter = new WeatherForecastAdapter(cityForecast.getForecastList());
        recyclerView.setAdapter(forecastAdapter);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }
}
