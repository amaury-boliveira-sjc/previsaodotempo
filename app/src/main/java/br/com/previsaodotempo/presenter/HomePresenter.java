package br.com.previsaodotempo.presenter;

import br.com.previsaodotempo.model.City;
import br.com.previsaodotempo.model.CityForecast;
import br.com.previsaodotempo.repository.HomeRepository;
import br.com.previsaodotempo.view.IHomeView;

public class HomePresenter {

    private IHomeView homeView;
    private HomeRepository homeRepository;

    public HomePresenter(IHomeView homeView, HomeRepository homeRepository) {
        this.homeView = homeView;
        this.homeRepository = homeRepository;
    }

    public void getCityID(String city){
        homeRepository.listCity(city, new HomeRepository.ListCityCallback() {
            @Override
            public void onSuccess(City city) {
                forecast(city);
            }

            @Override
            public void onFailure(String error) {
                homeView.showError(error);
            }
        });
    }

    private void forecast(City city){
        homeRepository.forecast(city.getId(), new HomeRepository.ForecastCallback() {
            @Override
            public void onSuccess(CityForecast cityForecast) {
                homeView.showForecasts(cityForecast);
            }

            @Override
            public void onFailure(String error) {
                homeView.showError(error);
            }
        });
    }
}
