package br.com.previsaodotempo.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "cidades", strict = false)
public class City implements Serializable {

    @Element
    @Path("./cidade")
    private long id;

    public City(long id) {
        this.id = id;
    }

    public City() {
    }

    public long getId() {
        return id;
    }
}
