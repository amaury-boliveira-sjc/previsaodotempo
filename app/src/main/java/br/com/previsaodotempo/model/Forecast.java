package br.com.previsaodotempo.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "previsao", strict = false)
public class Forecast implements Serializable {

    @Element(name = "dia")
    private String date;

    @Element(name = "tempo")
    private String condition;

    @Element(name = "maxima")
    private int maxTemperature;

    @Element(name = "minima")
    private int minTemperature;

    public Forecast(String date, String condition, int maxTemperature, int minTemperature) {
        this.date = date;
        this.condition = condition;
        this.maxTemperature = maxTemperature;
        this.minTemperature = minTemperature;
    }

    public Forecast() {
    }

    public String getDate() {
        return date;
    }

    public String getCondition() {
        return condition;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public int getMinTemperature() {
        return minTemperature;
    }
}
