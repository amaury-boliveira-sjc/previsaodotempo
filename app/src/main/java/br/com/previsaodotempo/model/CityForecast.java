package br.com.previsaodotempo.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

@Root(strict = false)
public class CityForecast implements Serializable {

    @Element(name = "nome")
    private String name;

    @Element(name = "atualizacao")
    private String updatedDate;

    @ElementList(inline = true)
    private List<Forecast> forecastList;

    public CityForecast(String name, String updatedDate, List<Forecast> forecastList) {
        this.name = name;
        this.updatedDate = updatedDate;
        this.forecastList = forecastList;
    }

    public CityForecast() {
    }

    public String getName() {
        return name;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public List<Forecast> getForecastList() {
        return forecastList;
    }

}
