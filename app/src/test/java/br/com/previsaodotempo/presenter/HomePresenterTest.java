package br.com.previsaodotempo.presenter;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import br.com.previsaodotempo.model.City;
import br.com.previsaodotempo.model.CityForecast;
import br.com.previsaodotempo.model.Forecast;
import br.com.previsaodotempo.repository.HomeRepository;
import br.com.previsaodotempo.view.IHomeView;

import static org.junit.Assert.assertEquals;

public class HomePresenterTest {

    private static final String CITY = "São josé dos Campos";
    private static final String ERROR = "Erro ao retornar previsão do tempo";
    private HomePresenter homePresenter;
    @Mock
    private IHomeView homeView;

    @Mock
    private HomeRepository homeRepository;

    @Captor
    private ArgumentCaptor<HomeRepository.ListCityCallback> listCityCallback;

    @Captor
    private ArgumentCaptor<HomeRepository.ForecastCallback> forecastCallback;

    //método beforeclass. Apenas para aprendizado
    @BeforeClass
    public static void beforeClass(){
    }

    @Before
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        this.homePresenter = new HomePresenter(homeView, homeRepository);
    }

    @Test
    public void testGetCityID(){
        homePresenter.getCityID(CITY);
        //verifica se realmente o presenter foi chamado passando as variáveis corretas
        Mockito.verify(homeRepository).listCity(Mockito.anyString(), listCityCallback.capture());
        listCityCallback.getValue().onSuccess(mockCity());

        Mockito.verify(homeRepository).forecast(Mockito.anyLong(), forecastCallback.capture());
        forecastCallback.getValue().onSuccess(mockCityForecast());

        ArgumentCaptor<CityForecast> cityForecastCaptor = ArgumentCaptor.forClass(CityForecast.class);
        Mockito.verify(homeView).showForecasts(cityForecastCaptor.capture());

        assertEquals(1, cityForecastCaptor.getValue().getForecastList().size());
    }

    @Test
    public void testListCityError(){
        homePresenter.getCityID(CITY);
        Mockito.verify(homeRepository).listCity(Mockito.anyString(), listCityCallback.capture());
        listCityCallback.getValue().onFailure(ERROR);

        ArgumentCaptor<String> errorCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(homeView).showError(errorCaptor.capture());
        assertEquals(ERROR, errorCaptor.getValue());
    }

    @Test
    public void testForecastError(){
        homePresenter.getCityID(CITY);
        Mockito.verify(homeRepository).listCity(Mockito.anyString(), listCityCallback.capture());
        listCityCallback.getValue().onSuccess(mockCity());

        Mockito.verify(homeRepository).forecast(Mockito.anyLong(), forecastCallback.capture());
        forecastCallback.getValue().onFailure(ERROR);

        ArgumentCaptor<String> errorCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(homeView).showError(errorCaptor.capture());
        assertEquals(ERROR, errorCaptor.getValue());
    }

    //Método que força o onSuccess do teste getCityID
    private City mockCity(){
        return new City(1234);
    }

    //Método que força o onSuccess do teste getCityID
    private CityForecast mockCityForecast(){
        List<Forecast> forecasts = new ArrayList<>();
        forecasts.add(new Forecast("13/10/2020", "chuva", 23, 14));
        return new CityForecast(CITY, "12/10/2020", forecasts);
    }

    //método after. Apenas para aprendizado
    @After
    public void tearDown(){
    }

    //método afterClass. Apenas para apredizado
    @AfterClass
    public static void afterClass(){
    }
}
