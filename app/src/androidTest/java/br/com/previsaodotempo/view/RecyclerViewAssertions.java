package br.com.previsaodotempo.view;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewAssertion;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import static org.junit.Assert.assertThat;

public class RecyclerViewAssertions implements ViewAssertion {

    private Matcher<Integer> matcher;

    public RecyclerViewAssertions(Matcher<Integer> matcher) {
        this.matcher = matcher;
    }

    public static RecyclerViewAssertions withItemCount(Matcher<Integer> matcher){
        return new RecyclerViewAssertions(matcher);
    }

    @Override
    public void check(View view, NoMatchingViewException noViewFoundException) {
        if (noViewFoundException != null){
            throw noViewFoundException;
        }
        RecyclerView recyclerView = (RecyclerView) view;
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        assertThat(adapter.getItemCount(), matcher);
    }
}
