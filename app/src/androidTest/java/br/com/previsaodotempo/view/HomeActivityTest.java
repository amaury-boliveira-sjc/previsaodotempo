package br.com.previsaodotempo.view;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.previsaodotempo.R;
import br.com.previsaodotempo.util.EspressoTestingIdlingResource;

@RunWith(AndroidJUnit4.class)
public class HomeActivityTest {

    @Rule
    public ActivityScenarioRule<HomeActivity> testRule = new ActivityScenarioRule<>(HomeActivity.class);

    @Before
    public void setUp(){
        IdlingRegistry.getInstance().register(EspressoTestingIdlingResource.getResource());

    }

    @Test
    public void testShowForecast(){
        Espresso.onView(ViewMatchers.withId(R.id.edit_text_city_name)).
                perform(ViewActions.typeText("Sao Jose dos Campos"));
        Espresso.onView(ViewMatchers.withId(R.id.button_search)).
                perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.text_view_city)).
                check(ViewAssertions.matches(Matchers.not(ViewMatchers.withText(""))));
        Espresso.onView(ViewMatchers.withId(R.id.text_view_updated)).
                check(ViewAssertions.matches(Matchers.not(ViewMatchers.withText(""))));
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view_weather_forecast)).
                check(RecyclerViewAssertions.withItemCount(Matchers.greaterThan(0)));
    }

    @After
    public void tearDown(){
        IdlingRegistry.getInstance().unregister(EspressoTestingIdlingResource.getResource());
    }

}
